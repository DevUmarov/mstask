package com.example.mstask.api.vo

import com.google.gson.annotations.SerializedName


data class Response (
    @SerializedName("searchSessionId") val searchSessionId : String,
    @SerializedName("cnt") val cnt : String,
    @SerializedName("users") val users : List<User>,
    @SerializedName("errors") val errors : List<String>,
    @SerializedName("status") val status : String
)