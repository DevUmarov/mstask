package com.example.mstask.api

import com.example.mstask.api.vo.Response
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET(EndPoints.USER_LIST)
    fun getUserList(@Query("page") page: Int): Observable<Response>
}