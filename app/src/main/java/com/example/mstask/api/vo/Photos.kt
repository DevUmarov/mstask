package com.example.mstask.api.vo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Photos(
    @SerializedName("errors") val errors: List<String>,
    @SerializedName("url") val url: String,
    @SerializedName("urlBig") val urlBig: String,
    @SerializedName("photo_id") val photo_id: Int,
    @SerializedName("liked") val liked: String?,
    @SerializedName("likedCount") val likedCount: String?,
    @SerializedName("descr") val descr: String?,
    @SerializedName("status") val status: String?
) : Parcelable