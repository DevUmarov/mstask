package com.example.mstask.api.datasource

import androidx.paging.DataSource
import com.example.mstask.api.ApiInterface
import com.example.mstask.api.vo.User
import io.reactivex.disposables.CompositeDisposable

class UserDataSourceFactory(
    private val apiInterface: ApiInterface,
    private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, User>() {
    override fun create(): DataSource<Int, User> = UserDataSource(apiInterface, compositeDisposable)
}