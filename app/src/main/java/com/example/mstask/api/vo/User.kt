package com.example.mstask.api.vo

import com.google.gson.annotations.SerializedName


data class User (
	@SerializedName("login") val login : String,
	@SerializedName("name") val name : String,
	@SerializedName("nick") val nick : String,
	@SerializedName("iurl") val iurl : String,
	@SerializedName("iurl_600") val iurl_600 : String,
	@SerializedName("iurl_200") val iurl_200 : String,
	@SerializedName("photos") val photos : List<Photos>,
	@SerializedName("online") val online : Boolean,
	@SerializedName("pcnt") val pcnt : Int,
	@SerializedName("age") val age : String,
	@SerializedName("city") val city : String,
	@SerializedName("aim") val aim : String,
	@SerializedName("photo_id") val photo_id : Int,
	@SerializedName("moderator") val moderator : Boolean,
	@SerializedName("status") val status : String,
	@SerializedName("greeting") val greeting : String,
	@SerializedName("lastVisit") val lastVisit : String,
	@SerializedName("sex") val sex : Int
)