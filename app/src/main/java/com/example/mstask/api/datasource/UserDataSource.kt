package com.example.mstask.api.datasource

import androidx.paging.PageKeyedDataSource
import com.example.mstask.api.ApiInterface
import com.example.mstask.api.vo.User
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class UserDataSource(
    private val apiService: ApiInterface,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, User>() {

    private var page = 1

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, User>
    ) {
        compositeDisposable.add(
            apiService.getUserList(page)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        callback.onResult(it.users, null, page + 1)
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        compositeDisposable.add(
            apiService.getUserList(params.key)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        callback.onResult(it.users, params.key + 1)
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {

    }
}
