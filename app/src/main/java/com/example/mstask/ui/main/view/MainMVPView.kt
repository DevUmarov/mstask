package com.example.mstask.ui.main.view

import androidx.paging.PagedList
import com.example.mstask.api.vo.User
import com.example.mstask.base.view.MVPView

interface MainMVPView : MVPView {
    fun displayUserList(userList: PagedList<User>)
}