package com.example.mstask.ui.profile.view

import android.os.Bundle
import com.example.mstask.R
import com.example.mstask.adapters.PhotosListAdapter
import com.example.mstask.api.vo.Photos
import com.example.mstask.base.view.BaseActivity
import com.example.mstask.ui.profile.presenter.ProfileMVPPresenter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList
import javax.inject.Inject


class ProfileActivity : BaseActivity(), ProfileMVPView {

    companion object {
        val USER_PHOTOS_EXTRA = "com.example.mstask.ui.profile.view.USER_PHOTOS_EXTRA"
    }

    @Inject
    lateinit var presenter: ProfileMVPPresenter<ProfileMVPView>
    lateinit var photosListAdapter: PhotosListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_activity)
        presenter.onAttach(this)

        if (intent?.hasExtra(USER_PHOTOS_EXTRA) == true) {
            intent?.getParcelableArrayListExtra<Photos>(USER_PHOTOS_EXTRA)?.let {
                initRecycler(it)
            }
        }
    }

    private fun initRecycler(photos: ArrayList<Photos>) {
        photosListAdapter = PhotosListAdapter()
        users_rv.adapter = photosListAdapter
        users_rv.setHasFixedSize(true)

        photosListAdapter.updateList(photos)
    }


    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

}