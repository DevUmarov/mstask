package com.example.mstask.ui.profile.presenter

import com.example.mstask.api.ApiInterface
import com.example.mstask.base.presenter.BasePresenter
import com.example.mstask.ui.main.presenter.MainMVPPresenter
import com.example.mstask.ui.main.view.MainMVPView
import com.example.mstask.ui.profile.view.ProfileMVPView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProfilePresenter<V : ProfileMVPView> @Inject internal constructor(
    disposable: CompositeDisposable,
    apiInterface: ApiInterface
) :
    BasePresenter<V>(disposable), ProfileMVPPresenter<V> {


}