package com.example.mstask.ui.profile

import com.example.mstask.ui.main.presenter.MainMVPPresenter
import com.example.mstask.ui.main.presenter.MainPresenter
import com.example.mstask.ui.main.view.MainMVPView
import com.example.mstask.ui.profile.presenter.ProfileMVPPresenter
import com.example.mstask.ui.profile.presenter.ProfilePresenter
import com.example.mstask.ui.profile.view.ProfileMVPView
import dagger.Module
import dagger.Provides

@Module
class ProfileActivityModule {

    @Provides
    internal fun provideProfilePresenter(profilePresenter: ProfilePresenter<ProfileMVPView>)
            : ProfileMVPPresenter<ProfileMVPView> = profilePresenter
}