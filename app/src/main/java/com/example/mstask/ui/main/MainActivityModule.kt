package com.example.mstask.ui.main

import com.example.mstask.ui.main.presenter.MainMVPPresenter
import com.example.mstask.ui.main.presenter.MainPresenter
import com.example.mstask.ui.main.view.MainMVPView
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainMVPView>)
            : MainMVPPresenter<MainMVPView> = mainPresenter
}