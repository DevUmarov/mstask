package com.example.mstask.ui.profile.presenter

import com.example.mstask.base.presenter.MVPPresenter
import com.example.mstask.ui.main.view.MainMVPView
import com.example.mstask.ui.profile.view.ProfileMVPView

interface ProfileMVPPresenter<V : ProfileMVPView> : MVPPresenter<V> {

}