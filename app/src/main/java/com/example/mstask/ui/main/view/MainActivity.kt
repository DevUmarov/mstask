package com.example.mstask.ui.main.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.paging.PagedList
import com.example.mstask.R
import com.example.mstask.adapters.OnItemClickCallback
import com.example.mstask.adapters.UsersPagedListAdapter
import com.example.mstask.api.vo.User
import com.example.mstask.base.view.BaseActivity
import com.example.mstask.ui.main.presenter.MainMVPPresenter
import com.example.mstask.ui.profile.view.ProfileActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity(), MainMVPView, OnItemClickCallback {

    @Inject
    lateinit var presenter: MainMVPPresenter<MainMVPView>
    lateinit var usersAdapter: UsersPagedListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        initRecycler()
        Log.d("TAG", "123");
        presenter.fetchUserList()
    }

    override fun onDestroy() {
        presenter.onDetach()
        Log.i("GAH", "321")
        super.onDestroy()
    }

    private fun initRecycler() {
        usersAdapter = UsersPagedListAdapter(this)
        users_rv.adapter = usersAdapter

        users_rv.setHasFixedSize(true)
    }

    override fun displayUserList(userList: PagedList<User>) {
        usersAdapter.submitList(userList)
    }

    override fun onItemClick(user: User) {
        startActivity(
            Intent(this, ProfileActivity::class.java)
                .apply {
                    val a = ArrayList(user.photos)
                    putParcelableArrayListExtra(ProfileActivity.USER_PHOTOS_EXTRA, a)
//                    putExtra(ProfileActivity.USER_PHOTOS_EXTRA, "asd")

                }
        )
    }
}
