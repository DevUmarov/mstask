package com.example.mstask.ui.main.presenter

import com.example.mstask.base.presenter.MVPPresenter
import com.example.mstask.ui.main.view.MainMVPView

interface MainMVPPresenter<V : MainMVPView> : MVPPresenter<V> {
    fun fetchUserList()
}