package com.example.mstask.ui.main.presenter

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.example.mstask.api.ApiInterface
import com.example.mstask.api.datasource.UserDataSourceFactory
import com.example.mstask.api.vo.User
import com.example.mstask.base.presenter.BasePresenter
import com.example.mstask.ui.main.view.MainMVPView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter<V : MainMVPView> @Inject internal constructor(
    disposable: CompositeDisposable,
    apiInterface: ApiInterface
) :
    BasePresenter<V>(disposable), MainMVPPresenter<V> {
    var userList: Observable<PagedList<User>>
    private val pagedSize = 20

    init {
        val config: PagedList.Config = PagedList.Config.Builder()
            .setPageSize(pagedSize)
            .setInitialLoadSizeHint(pagedSize * 3)
            .build()

        userList = RxPagedListBuilder(
            UserDataSourceFactory(apiInterface, compositeDisposable),
            config
        ).buildObservable()
    }

    override fun fetchUserList() {
        compositeDisposable.add(
            userList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pagedList: PagedList<User> ->
                    getView()?.displayUserList(pagedList)
                }, { error ->
                    getView()?.showToast(error.toString())
                })
        )
    }
}