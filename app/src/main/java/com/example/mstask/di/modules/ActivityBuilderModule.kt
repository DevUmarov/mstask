package com.example.mstask.di.modules

import com.example.mstask.di.annotations.ActivityScope
import com.example.mstask.ui.main.MainActivityModule
import com.example.mstask.ui.main.view.MainActivity
import com.example.mstask.ui.profile.ProfileActivityModule
import com.example.mstask.ui.profile.view.ProfileActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun MainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ProfileActivityModule::class])
    abstract fun ProfileActivity(): ProfileActivity
}