package com.example.mstask.base.view

interface MVPView {
    fun showToast(message: String)
}