package com.example.mstask.base.presenter

import com.example.mstask.base.view.MVPView


interface MVPPresenter<V : MVPView> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}