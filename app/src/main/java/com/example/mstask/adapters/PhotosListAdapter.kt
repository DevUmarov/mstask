package com.example.mstask.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mstask.R
import com.example.mstask.api.vo.Photos
import kotlinx.android.synthetic.main.item_photo_list.view.*

class PhotosListAdapter : RecyclerView.Adapter<PhotosListAdapter.PhotoViewHolder>() {

    private val photoList: ArrayList<Photos> = arrayListOf()

    fun updateList(list: ArrayList<Photos>) {
        photoList.clear()
        photoList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return PhotoViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo_list, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(photoList[position])
    }

    override fun getItemCount(): Int = photoList.size

    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(photo: Photos?) {
            Glide.with(itemView.context)
                .load(photo?.url)
                .into(itemView.photo_iv)
        }
    }
}