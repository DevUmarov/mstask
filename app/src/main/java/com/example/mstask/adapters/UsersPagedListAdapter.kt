package com.example.mstask.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mstask.R
import com.example.mstask.api.vo.User
import kotlinx.android.synthetic.main.item_user_list.view.*

interface OnItemClickCallback {
    fun onItemClick(user: User)
}


class UsersPagedListAdapter(private val onItemClickCallback: OnItemClickCallback) :
    PagedListAdapter<User, UsersPagedListAdapter.UserItemViewHolder>(UsersDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UsersPagedListAdapter.UserItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_user_list, parent, false)
        return UserItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: UsersPagedListAdapter.UserItemViewHolder, position: Int) {
        holder.bind(getItem(position), onItemClickCallback)
    }

    class UsersDiffCallback : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.login == newItem.login
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }

    class UserItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(user: User?, onItemClickCallback: OnItemClickCallback) {
            itemView.name_tv.text = user?.name
            itemView.age_tv.text = user?.age
            itemView.city_tv.text = user?.city

            Glide.with(itemView.context)
                .load(user?.iurl)
                .into(itemView.image_iv);

            itemView.setOnClickListener {
                user?.let {
                    onItemClickCallback.onItemClick(it)
                }
            }
        }
    }
}